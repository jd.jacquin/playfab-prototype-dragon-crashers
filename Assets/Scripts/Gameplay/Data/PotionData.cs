using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace UIToolkitDemo
{
    public class PotionData : MonoBehaviour
    {
        public static event Action<int> PotionsUpdated;

        [SerializeField] uint m_MaxHealingPotions = 3;
        [SerializeField] GameDataManager gameDataManager;

        private uint m_HealingPotionCount;
        public uint HealingPotionCount => m_HealingPotionCount;


        void OnEnable()
        {
            // listen for healed unit
            HealDropZone.UseOnePotion += OnUseOnePotion;
        }

        void OnDisable()
        {
            HealDropZone.UseOnePotion -= OnUseOnePotion;
        }

        private void Awake()
        {
            PlayFabManager.instance.onDataInit.AddListener(PotionInit);
        }

        void PotionInit()
        {
            m_HealingPotionCount = gameDataManager.GameData.healthPotions;
            PotionsUpdated?.Invoke((int)m_HealingPotionCount);
            PlayFabManager.instance.playerResources.onItemExpended.AddListener(PotionUseProcessed);
        }

        void UsePotion()
        {
            if (PlayFabManager.instance.playerResources.IsConsumingResources)
                return;

            PlayFabManager.instance.playerResources.ExpendItem(PlayFabManager.instance.playerResources.IdHealhPotion, 1);
        }

        // event-handling methods
        void OnUseOnePotion()
        {
            UsePotion();
        }

        void PotionUseProcessed(string itemId, bool success)
        {
            if (itemId == PlayFabManager.instance.playerResources.IdHealhPotion &&  success)
            {
                m_HealingPotionCount--;

                // notify the UI 
                PotionsUpdated?.Invoke((int)m_HealingPotionCount);
                AudioManager.PlayPotionDropSound();
            }
        }
    }
}
