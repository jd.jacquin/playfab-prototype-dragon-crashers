using System.Collections;
using System.Collections.Generic;
using UIToolkitDemo;
using UnityEngine;
using Utilities.Inspector;
using System;

public enum CutsceneMode
{
    Play,
    None
}

public class BattleGameplayManager : MonoBehaviour
{
    public static event Action GameWon;
    public static event Action GameLost;

    [Header("Teams")]
    public List<UnitController> heroTeamUnits;
    public List<UnitController> enemyTeamUnits;

    [Header("Team Logic")]
    public bool autoAssignUnitTeamTargets = false;

    //Runtime Battle Logic
    private List<UnitController> aliveHeroUnits;
    private List<UnitController> aliveEnemyUnits;

    [Header("Battle Intro")]
    public CutsceneMode introCutscene;
    public CutsceneTimelineBehaviour introCutsceneBehaviour;

    [Header("Battle Ended - Victory")]
    public CutsceneTimelineBehaviour victoryCutsceneBehaviour;
    public SceneField victoryNextScene;

    [Header("Battle Ended - Defeat")]
    public CutsceneTimelineBehaviour defeatCutsceneBehaviour;
    public SceneField defeatNextScene;

    private SceneField selectedNextScene;




    void Awake()
    {
        if (PlayFabManager.instance)
            PlayFabManager.instance.playerData.InitCombatManager(this);
    }

    void SetupTeamUnits()
    {
        CreateAliveUnits();

        if (autoAssignUnitTeamTargets)
        {
            AutoAssignUnitTeamTargets();
        }
    }

    void CreateAliveUnits()
    {
        aliveHeroUnits = new List<UnitController>();

        for (int i = 0; i < heroTeamUnits.Count; i++)
        {
            aliveHeroUnits.Add(heroTeamUnits[i]);
            aliveHeroUnits[i].SetAlive();
            aliveHeroUnits[i].UnitDiedEvent += UnitHasDied;
        }

        aliveEnemyUnits = new List<UnitController>();

        for (int i = 0; i < enemyTeamUnits.Count; i++)
        {
            aliveEnemyUnits.Add(enemyTeamUnits[i]);
            aliveEnemyUnits[i].SetAlive();
            aliveEnemyUnits[i].UnitDiedEvent += UnitHasDied;
        }
    }

    public void LoadData(BattleData battleData)
    {
        if (battleData.heroUnits == null)
        {
            Debug.Log("loading data - default");
            SetupTeamUnits(); // setup units normally
        }
        else
        {
            Debug.Log("loading data - server");
            aliveHeroUnits = new List<UnitController>();
            aliveEnemyUnits = new List<UnitController>();

            LoadUnitData(battleData.heroUnits, ref heroTeamUnits, true);
            LoadUnitData(battleData.enemyUnits, ref enemyTeamUnits, false);

            if (autoAssignUnitTeamTargets)
                AutoAssignUnitTeamTargets();
        }
        StartGameLogic();
        CheckRemainingTeams();
    }

    private void LoadUnitData(UnitData[] unitData, ref List<UnitController> unitControllers, bool isHeroUnits)
    {

        // !! assert, the order of hero units and abilities are loaded in the exact order they are saved, or else, would need to find the index by comparing the name of the hero/ability from the saved data to the given heroTeamUnits

        for (int i = 0; i < unitData.Length; i++)
        {
            if (unitData[i].health > 0)
            {

                if (isHeroUnits)
                    aliveHeroUnits.Add(unitControllers[i]);
                else
                    aliveEnemyUnits.Add(unitControllers[i]);

                unitControllers[i].SetAlive();
                unitControllers[i].healthBehaviour.ChangeHealth(-unitControllers[i].GetCurrentHealth() + unitData[i].health);
                unitControllers[i].UnitDiedEvent += UnitHasDied;
            }
            else
                unitControllers[i].UnitHasDied();
        }
    }


    void AutoAssignUnitTeamTargets()
    {
        for (int i = 0; i < aliveHeroUnits.Count; i++)
        {
            aliveHeroUnits[i].AssignTargetUnits(aliveEnemyUnits);
        }

        for (int i = 0; i < aliveEnemyUnits.Count; i++)
        {
            aliveEnemyUnits[i].AssignTargetUnits(aliveHeroUnits);
        }
    }

    void StartGameLogic()
    {
        switch (introCutscene)
        {
            case CutsceneMode.Play:
                StartIntroCutscene();
                break;

            case CutsceneMode.None:
                StartBattle();
                break;
        }
    }

    void StartIntroCutscene()
    {
        introCutsceneBehaviour.StartTimeline();
    }

    public void StartBattle()
    {
        for (int i = 0; i < aliveHeroUnits.Count; i++)
        {
            aliveHeroUnits[i].BattleStarted();
        }

        for (int i = 0; i < aliveEnemyUnits.Count; i++)
        {
            aliveEnemyUnits[i].BattleStarted();
        }
    }

    void UnitHasDied(UnitController deadUnit)
    {
        RemoveUnitFromAliveUnits(deadUnit);
    }

    void RemoveUnitFromAliveUnits(UnitController unit)
    {
        CheckRemainingTeams();
        for (int i = 0; i < aliveHeroUnits.Count; i++)
        {
            if (unit == aliveHeroUnits[i])
            {
                aliveHeroUnits[i].UnitDiedEvent -= UnitHasDied;
                aliveHeroUnits.RemoveAt(i);
                RemoveUnitFromEnemyTeamTargets(unit);
            }
        }

        if (aliveHeroUnits.Count > 0)
        {
            for (int i = 0; i < aliveEnemyUnits.Count; i++)
            {
                if (unit == aliveEnemyUnits[i])
                {
                    aliveEnemyUnits[i].UnitDiedEvent -= UnitHasDied;
                    aliveEnemyUnits.RemoveAt(i);
                    RemoveUnitFromHeroTeamTargets(unit);
                }
            }
        }

        CheckRemainingTeams();
    }

    void RemoveUnitFromHeroTeamTargets(UnitController unit)
    {
        for (int i = 0; i < aliveHeroUnits.Count; i++)
        {
            aliveHeroUnits[i].RemoveTargetUnit(unit);
        }
    }

    void RemoveUnitFromEnemyTeamTargets(UnitController unit)
    {
        for (int i = 0; i < aliveEnemyUnits.Count; i++)
        {
            aliveEnemyUnits[i].RemoveTargetUnit(unit);
        }
    }

    void CheckRemainingTeams()
    {

        if (aliveHeroUnits.Count == 0)
            SetBattleDefeat();

        else if (aliveEnemyUnits.Count == 0)
            SetBattleVictory();
    }

    void SetBattleVictory()
    {
        StopAllAliveTeamUnits(aliveHeroUnits);

        if (victoryCutsceneBehaviour != null)
        {
            victoryCutsceneBehaviour.StartTimeline();
        }

        // notify GameScreen to show the Victory UI
        GameWon?.Invoke();

    }

    void SetBattleDefeat()
    {
        StopAllAliveTeamUnits(aliveEnemyUnits);

        if (defeatCutsceneBehaviour != null)
        {
            defeatCutsceneBehaviour.StartTimeline();
        }

        // notify the GameScreen to show the Defeat UI
        GameLost?.Invoke();
    }

    void StopAllAliveTeamUnits(List<UnitController> aliveTeamUnits)
    {
        for (int i = 0; i < aliveTeamUnits.Count; i++)
        {
            aliveTeamUnits[i].BattleEnded();
        }
    }

    public void SelectVictoryNextScene()
    {
        selectedNextScene = victoryNextScene;

    }

    public void SelectDefeatNextScene()
    {
        selectedNextScene = defeatNextScene;
    }

    public void LoadSelectedScene()
    {
        NextSceneLoader sceneLoader = new NextSceneLoader();
        sceneLoader.LoadNextScene(selectedNextScene);
    }

}

