using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

namespace UIToolkitDemo
{
    [System.Serializable]
    public struct BattleData
    {
        public UnitData[] heroUnits;
        public UnitData[] enemyUnits;
    }

    [System.Serializable]
    public struct UnitData
    {
        public int health;
    }
}
