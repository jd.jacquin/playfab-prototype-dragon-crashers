using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UIToolkitDemo
{
    public class CombatDataManager : MonoBehaviour
    {
        [SerializeField] private Button btnDebugDataSaver;
        [SerializeField] BattleGameplayManager gameplayManager;

        void Awake()
        {
            if (PlayFabManager.instance)
            {
                btnDebugDataSaver.onClick.AddListener(PlayFabManager.instance.playerData.SaveBattleData);
                PlayFabManager.instance.playerData.LoadPlayerData += gameplayManager.LoadData;
                PlayFabManager.instance.playerData.LoadBattleData();
            }
            else
            {
                gameplayManager.LoadData(new BattleData());
            }
        }
    }
}
