using PlayFab.DataModels;
using PlayFab;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using System;

namespace UIToolkitDemo
{
    public class PlayFabData : MonoBehaviour
    {
        const string COMBAT_DATA_NAME = "Combat_Data";

        private BattleGameplayManager gameplayManager;

        public event Action<BattleData> LoadPlayerData;

        void Start()
        {
            DontDestroyOnLoad(gameObject);
        }

        public void InitCombatManager(BattleGameplayManager gameplayManager)
        {
            this.gameplayManager = gameplayManager;
        }

        // -------------------------------------------------------- Saving --------------------------------------------------------

        public void SaveBattleData()
        {
            BattleData battleData = new BattleData();
            battleData.heroUnits = new UnitData[gameplayManager.heroTeamUnits.Count];
            battleData.enemyUnits = new UnitData[gameplayManager.enemyTeamUnits.Count];

            PopulateBattleData(ref battleData, gameplayManager.heroTeamUnits, true);
            PopulateBattleData(ref battleData, gameplayManager.enemyTeamUnits, false);
            UploadBattleData(PlayFabManager.instance.PlayerEntityKey, JsonUtility.ToJson(battleData));
        }

        private void PopulateBattleData(ref BattleData battleData, List<UnitController> units, bool isHeroUnits)
        {
            for (int i = 0; i < units.Count; i++)
            {
                int health = units[i].GetCurrentHealth();

                UnitData unitData = new UnitData();
                unitData.health = health;

                if (isHeroUnits)
                    battleData.heroUnits[i] = unitData;
                else
                    battleData.enemyUnits[i] = unitData;
            }
        }



        private void UploadBattleData(EntityKey entityKey, string jsonData)
        {
            List<SetObject> setObjects = new List<SetObject>();
            SetObject newSetObject = new SetObject
            {
                DeleteObject = false,
                EscapedDataObject = jsonData,
                ObjectName = COMBAT_DATA_NAME
            };
            setObjects.Add(newSetObject);

            var request = new SetObjectsRequest { Entity = entityKey, Objects = setObjects };
            PlayFabDataAPI.SetObjects(request, OnSetObjects, SetObjectsError);
        }

        private void OnSetObjects(SetObjectsResponse response)
        {
            Debug.Log("Set objects");
            // exit battle or what not
        }

        private void SetObjectsError(PlayFabError error)
        {
            Debug.LogError("Failed to set objects:");
            Debug.LogError(error.GenerateErrorReport());
        }

        // -------------------------------------------------------- Loading --------------------------------------------------------

        public void LoadBattleData()
        {
            var request = new GetObjectsRequest { Entity = PlayFabManager.instance.PlayerEntityKey, EscapeObject = true };
            PlayFabDataAPI.GetObjects(request, OnGetObjects, GetObjectsError);
        }

        private void OnGetObjects(GetObjectsResponse response)
        {
            Debug.Log("get objects");
            if (response.Objects.TryGetValue(COMBAT_DATA_NAME, out ObjectResult objectResult))
            {
                BattleData battleData = JsonUtility.FromJson<BattleData>(objectResult.EscapedDataObject);
                LoadPlayerData?.Invoke(battleData);
            }
            else{
                LoadPlayerData?.Invoke(new BattleData());
            }
        }

        private void GetObjectsError(PlayFabError error)
        {
            Debug.LogError("Failed to get objects:");
            Debug.LogError(error.GenerateErrorReport());
            LoadPlayerData?.Invoke(new BattleData());
        }
    }
}
