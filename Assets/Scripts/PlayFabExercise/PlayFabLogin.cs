using PlayFab.ClientModels;
using PlayFab;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.UI;
using TMPro;

namespace UIToolkitDemo
{
    public class PlayFabLogin : MonoBehaviour
    {

        [SerializeField] private GameObject objLogin;
        [SerializeField] private UnityEngine.UI.Button btnLogin;
        [SerializeField] private TMP_InputField inputFldLogin;

        [SerializeField] private GameDataManager gameDataManager;

        public void Start()
        {
            if (string.IsNullOrEmpty(PlayFabSettings.staticSettings.TitleId))
                PlayFabSettings.staticSettings.TitleId = "CE5F6";

            btnLogin.onClick.AddListener(AttemptLogin);
        }

        private void AttemptLogin()
        {
            if (inputFldLogin != null)
            {
                btnLogin.interactable = false;
                inputFldLogin.interactable = false;
                var request = new LoginWithCustomIDRequest { CustomId = inputFldLogin.text, CreateAccount = true };
                PlayFabClientAPI.LoginWithCustomID(request, OnLoginSuccess, OnLoginFailure);
            }
            else
                Debug.LogWarning("Login cannot be empty");
        }



        private void OnLoginSuccess(LoginResult result)
        {
            Debug.Log("Congratulations, you made your first successful API call!");
            objLogin.SetActive(false);

            PlayFabManager.instance.LoginComplete(inputFldLogin.text, new PlayFab.DataModels.EntityKey { Id = result.EntityToken.Entity.Id, Type = result.EntityToken.Entity.Type });
        }

        private void OnLoginFailure(PlayFabError error)
        {
            Debug.LogWarning("Something went wrong with your first API call.  :(");
            Debug.LogError("Here's some debug information:");
            Debug.LogError(error.GenerateErrorReport());
            btnLogin.interactable = true;
            inputFldLogin.interactable = true;
        }

    }
}
