using PlayFab.DataModels;
using UnityEngine;
using UnityEngine.Events;

namespace UIToolkitDemo
{
    public class PlayFabManager : MonoBehaviour
    {
        public static PlayFabManager instance;

        public PlayFabPlayerResources playerResources;
        public PlayFabData playerData;
        
        [SerializeField] private GameObject objOverlay;

        private GameDataManager gameDataManager;

        private bool loggedIn = false;
        private string trackedId;
        private EntityKey playerEntityKey;

        public UnityEvent onDataInit;

        public EntityKey PlayerEntityKey { get { return playerEntityKey; } }

        void Awake()
        {
            if (instance == null)
                instance = this;
            else
            {
                Destroy(playerResources);
                Destroy(this);
            }

            DontDestroyOnLoad(gameObject);
        }

        public void InitData(GameDataManager gameDataManager)
        {
            this.gameDataManager = gameDataManager;
            if (loggedIn)
            {
                gameDataManager.GameData.username = trackedId;
                playerResources.GetInventory();
                //onDataInit?.Invoke();
                Debug.Log("listener invoke");
            }
        }

        public void LoginComplete(string id, EntityKey entityKey)
        {
            gameDataManager.GameData.username = id;
            trackedId = id;
            playerEntityKey = entityKey;
            playerResources.GetInventory();
            loggedIn = true;
        }

        public void ResourcesGot(int gold, int gems, int healthPotions, int powerPotions)
        {
            gameDataManager.GameData.gold = (uint)gold;
            gameDataManager.GameData.gems = (uint)gems;
            gameDataManager.GameData.healthPotions = (uint)healthPotions;
            gameDataManager.GameData.levelUpPotions = (uint)powerPotions;
            gameDataManager.InitData();
            if (objOverlay)
                objOverlay.SetActive(false);

            onDataInit?.Invoke();
        }
    }
}
