using PlayFab.ClientModels;
using PlayFab;
using UnityEngine;
using UnityEngine.Events;
using UnityEditor.PackageManager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

namespace UIToolkitDemo
{
    public class PlayFabPlayerResources : MonoBehaviour
    {
        [SerializeField] private Button btnDebugLeaderboard;

        const string ID_GOLD = "GD";
        const string ID_GEMS = "GM";
        const string ID_HEALTH_POTIONS = "hpPotion";
        const string ID_POWER_POTIONS = "puPotion";

        const string ID_TRACKED_STATISTIC = "Richest Players";

        private bool isProcessingResources = false;
        private bool isConsumingResources = false;

        public bool IsProccessingResources {  get { return isProcessingResources; } }
        public bool IsConsumingResources { get { return isConsumingResources; } }

        public string IdGold { get { return ID_GOLD; } }
        public string IdGems { get { return ID_GEMS; } }
        public string IdHealhPotion { get { return ID_HEALTH_POTIONS; } }
        public string IdPowerPotion { get { return ID_POWER_POTIONS; } }

        private int curContentValue;
        private string curItemExpendId;
        private int curConsumeAmount;
        private int curGold;

        /// <summary>
        /// On purchase processed, invoke a boolean that indicates if the purchase was successful
        /// </summary>
        public UnityEvent<bool> onPurchaseProcessed;

        public UnityEvent<string, bool> onItemExpended;

        void Start()
        {
            if (btnDebugLeaderboard)
                btnDebugLeaderboard.onClick.AddListener(OutputLeaderboard);
            DontDestroyOnLoad(gameObject);
        }



        // -------------------------------------------------------- Inventory --------------------------------------------------------

        public void GetInventory()
        {
            var request = new GetUserInventoryRequest { };
            PlayFabClientAPI.GetUserInventory(request, OnGetInventorySuccess, OnGetInventoryError);
        }

        private void OnGetInventorySuccess(GetUserInventoryResult result)
        {
            int hpPotions = 0;
            int puPotions = 0;
            for (int i = 0; i < result.Inventory.Count; i++)
            {
                if (result.Inventory[i].ItemId == ID_HEALTH_POTIONS) { hpPotions += (int)result.Inventory[i].RemainingUses; }
                else if (result.Inventory[i].ItemId == ID_POWER_POTIONS) { puPotions += (int)result.Inventory[i].RemainingUses; }
            }
            PlayFabManager.instance.ResourcesGot(result.VirtualCurrency[ID_GOLD], result.VirtualCurrency[ID_GEMS], hpPotions, puPotions);
        }

        private void OnGetInventoryError(PlayFabError error)
        {
            GenericError(error, "Unable to get player inventory:");

            if (isConsumingResources)
            {
                isConsumingResources = false;
                onItemExpended?.Invoke(curItemExpendId, false);
            }
        }



        // -------------------------------------------------------- Item Usage --------------------------------------------------------

        public void ExpendItem(string itemId, int consumeAmount)
        {
            isConsumingResources = true;
            curItemExpendId = itemId;
            curConsumeAmount = consumeAmount;

            var request = new GetUserInventoryRequest { };
            PlayFabClientAPI.GetUserInventory(request, GetItemInstanceId, OnGetInventoryError);
        }

        private void GetItemInstanceId(GetUserInventoryResult result)
        {
            string instanceId = "";
            int consumeAmount = curConsumeAmount;
            for (int i = 0; i < result.Inventory.Count; i++)
            {
                if (result.Inventory[i].ItemId == curItemExpendId)
                {
                    instanceId = result.Inventory[i].ItemInstanceId;
                    // usesLeft = (int)result.Inventory[i].RemainingUses;
                    if (curConsumeAmount > (int)result.Inventory[i].RemainingUses)
                        consumeAmount = (int)result.Inventory[i].RemainingUses;
                    break;
                }
            }

            if (instanceId == "")
            {
                isConsumingResources = false;
                onItemExpended?.Invoke(curItemExpendId, false);
                return;
            }

            curConsumeAmount -= consumeAmount;

            Debug.Log($"Consuming item, id [{instanceId}], curConsume[{curConsumeAmount}], consumeAmount[{consumeAmount}]");

            var request = new ConsumeItemRequest { ItemInstanceId = instanceId, ConsumeCount = consumeAmount };
            PlayFabClientAPI.ConsumeItem(request, OnConsumeSuccess, OnConsumeError);
        }

        private void OnConsumeSuccess(ConsumeItemResult result)
        {

            // Loop back until the approriate amount of consumption has occured
            if (curConsumeAmount > 0)
            {
                var request = new GetUserInventoryRequest { };
                PlayFabClientAPI.GetUserInventory(request, GetItemInstanceId, OnGetInventoryError);
                return;
            }

            isConsumingResources = false;
            onItemExpended?.Invoke(curItemExpendId, true);
        }

        private void OnConsumeError(PlayFabError error)
        {
            GenericError(error, "Failure to consume resource");
            //onPurchaseProcessed?.Invoke(false);
            isConsumingResources = false;
            onItemExpended?.Invoke(curItemExpendId, false);
        }



        // -------------------------------------------------------- Purchasing --------------------------------------------------------

        public void PayForItem(string itemId, string currencyId, int expectedPrice)
        {
            isProcessingResources = true;
            var request = new PurchaseItemRequest { ItemId = itemId, VirtualCurrency = currencyId, Price = expectedPrice };
            PlayFabClientAPI.PurchaseItem(request, OnPurchaseItemSuccess, OnPurchaseItemError);
        }

        public void PayForCurrency(string currencyToBuyID, string paymentId, int price, int contentValue)
        {
            isProcessingResources = true;
            // Skip straight to adding resource if no price or buying gems (since it uses real money)
            if (currencyToBuyID == ID_GEMS || price <= 0)
            {
                var addRequest = new AddUserVirtualCurrencyRequest { VirtualCurrency = currencyToBuyID, Amount = contentValue };
                PlayFabClientAPI.AddUserVirtualCurrency(addRequest, OnAddCurrencySuccess, OnPurchaseItemError);
                return;
            }
            curContentValue = contentValue;
            var request = new SubtractUserVirtualCurrencyRequest { VirtualCurrency = paymentId, Amount = price };
            PlayFabClientAPI.SubtractUserVirtualCurrency(request, OnSubtractCurrencySuccess, OnPurchaseItemError);
        }

        private void OnPurchaseItemSuccess(PurchaseItemResult result)
        {
            Debug.Log("Item successfully purchased");
            onPurchaseProcessed?.Invoke(true);
            isProcessingResources = false;
        }

        private void OnPurchaseItemError(PlayFabError error)
        {
            GenericError(error, "Failure to process resources:");
            onPurchaseProcessed?.Invoke(false);
            isProcessingResources = false;
        }

        private void OnSubtractCurrencySuccess(ModifyUserVirtualCurrencyResult result)
        {
            Debug.Log("Currency successfully subtracted");
            string currencyToAddId = "";
            if (result.VirtualCurrency == ID_GEMS)
                currencyToAddId = ID_GOLD;

            var request = new AddUserVirtualCurrencyRequest { VirtualCurrency = currencyToAddId, Amount = curContentValue };
            PlayFabClientAPI.AddUserVirtualCurrency(request, OnAddCurrencySuccess, OnPurchaseItemError);
        }

        private void OnAddCurrencySuccess(ModifyUserVirtualCurrencyResult result)
        {
            Debug.Log("Currency successfully added");
            onPurchaseProcessed?.Invoke(true);
            isProcessingResources = false;

            if (result.VirtualCurrency == ID_GOLD)
                GoldIncreased(result.Balance);
        }



        // -------------------------------------------------------- Leaderboard --------------------------------------------------------

        private void GoldIncreased(int newGold)
        {
            curGold = newGold;
            var request = new GetPlayerStatisticsRequest { };
            PlayFabClientAPI.GetPlayerStatistics(request, OnGetPlayerStatistics, (PlayFabError error) => GenericError(error, "Failure to get player stats"));
        }

        public void OutputLeaderboard()
        {
            var request = new GetLeaderboardRequest { StatisticName = ID_TRACKED_STATISTIC };
            PlayFabClientAPI.GetLeaderboard(request, OnGetLeaderboard, (PlayFabError error) => GenericError(error, "Failure to get leaderboard"));
        }

        private void OnGetPlayerStatistics(GetPlayerStatisticsResult result)
        {
            for (int i = 0; i < result.Statistics.Count; i++)
            {
                Debug.Log(result.Statistics[i].StatisticName);
                if (result.Statistics[i].StatisticName == ID_TRACKED_STATISTIC)
                {
                    if (result.Statistics[i].Value < curGold)
                    {
                        List<StatisticUpdate> statisticUpdates = new List<StatisticUpdate> { new StatisticUpdate { StatisticName = ID_TRACKED_STATISTIC, Value = curGold }};
                        var request = new UpdatePlayerStatisticsRequest { Statistics = statisticUpdates };
                        PlayFabClientAPI.UpdatePlayerStatistics(request, (UpdatePlayerStatisticsResult result) => Debug.Log("Player stats have been updated") , (PlayFabError error) => GenericError(error, "Failure to update player stats"));
                    }
                    return;
                }
            }
            // if statistic wasn't found, player is currently not on leaderboard, therefore add them
            List<StatisticUpdate> newStatisticUpdates = new List<StatisticUpdate> { new StatisticUpdate { StatisticName = ID_TRACKED_STATISTIC, Value = curGold } };
            var newRequest = new UpdatePlayerStatisticsRequest { Statistics = newStatisticUpdates };
            PlayFabClientAPI.UpdatePlayerStatistics(newRequest, (UpdatePlayerStatisticsResult result) => Debug.Log("Player stats have been updated"), (PlayFabError error) => GenericError(error, "Failure to update player stats"));
        }

        private void OnGetLeaderboard(GetLeaderboardResult result)
        {
            Debug.Log("Leaderboard:");
            for (int i = 0; i < result.Leaderboard.Count; i++)
            {
                Debug.Log($"[{result.Leaderboard[i].Position}] {result.Leaderboard[i].PlayFabId} : {result.Leaderboard[i].StatValue}");
            }
        }

        // -------------------------------------------------------- Other --------------------------------------------------------

        private void GenericError(PlayFabError error, string msg)
        {
            Debug.LogError(msg);
            Debug.LogError(error.GenerateErrorReport());
        }
    }
}
